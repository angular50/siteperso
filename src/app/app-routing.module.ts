import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { WelcomeComponent } from "./welcome/welcome.component";
import { AboutComponent } from "./about/about.component";
import { WorkComponent } from "./work/work.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { ResumeComponent } from "./resume/resume.component";

const routes: Routes = [
  { path: "", component: WelcomeComponent },
  { path: "about", component: AboutComponent },
  { path: "work", component: WorkComponent },
  { path: "resume", component: ResumeComponent },
  { path: "**", component: NotFoundComponent }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
