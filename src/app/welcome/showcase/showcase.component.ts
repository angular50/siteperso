import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showcase',
  templateUrl: './showcase.component.html',
  styleUrls: ['./showcase.component.scss']
})
export class ShowcaseComponent implements OnInit {
  public aboutInfo = [
    {
      frontText: 'Tech Stack',
      frontIcon: 'fas fa-laptop',
      rearText:
        'Lorem ipsum'
    },
    {
      frontText: 'Learning Process',
      frontIcon: 'fas fa-book-open',
      rearText:
      'Lorem ipsum'
    },
    {
      frontText: 'Hobbies',
      frontIcon: 'fas fa-running',
      rearText:
      'Lorem ipsum'
    }
  ];

  constructor() {}

  ngOnInit(): void {}
}
