import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.scss']
})
export class WorkComponent implements OnInit {
  projects = [
    {
      title: 'Gestibank',
      description: [
        'Création d’une application de gestion bancaire en respectant un cahier des charges. Conception libre de l’architecture avec recommandations pour l’utilisation des outils de développement.'
      ],
      video: 'assets/video/demoGestibank.mp4',
      technologies: ['SpringBoot', 'MySQL', 'Angular'],
      image: 'assets/img/gestibank.jpg',
      gitlab: {
        code: 'https://gitlab.com/mustapha_el_khalfaoui/front-angular-bank-v2'
      },
      link: 'https://appbank.netlify.app/'
    },
    {
      title: 'JavAlumni',
      description: [
        'Développement d\'une application de mise en relation des anciens et futurs bénéficiaires des POEC. Interface de gestion des projets / articles / Quizs / FAQs et Events'
      ],
      video: '',
      technologies: ['SpringBoot', 'MySQL', 'Angular'],
      image: 'assets/img/javalumni.jpg',
      gitlab: {
        code: 'https://gitlab.com/mustapha_el_khalfaoui/front-angular-javalumni'
      },
      link: 'https://javalumni.netlify.app/'
    },
    {
      title: 'IonicRecettes',
      description: [
        'Développement d\'une application mobile listant des recettes orientales'
      ],
      video: '',
      technologies: ['Ionic'],
      image: 'assets/img/ionicRecettes.jpg',
      gitlab: {
        code: 'https://gitlab.com/mustapha_el_khalfaoui/ionicrecettes'
      },
      link: 'https://recettesfat.netlify.app/books'
    },
    {
      title: 'MarvelQuiz',
      description: [
        'Développement d\'une application quiz dans la thématique Marvel'
      ],
      video: '',
      technologies: ['React'],
      image: 'assets/img/quizMarvel.jpg',
      gitlab: {
        code: 'https://gitlab.com/react56/quiz'
      },
      link: 'https://quiz-7cc05.firebaseapp.com/'
    },
    {
      title: 'YelpHoceima',
      description: [
        'Développement d\'une application d\’avis sur des lieux touristiques de la ville d\’Al Hoceima'
      ],
      video: '',
      technologies: ['React'],
      image: 'assets/img/yelp.jpg',
      gitlab: {
        code: 'https://gitlab.com/react56/beachyelpclonereact'
      },
      link: 'https://yelp-4f62d.web.app/'
    },
    {
      title: 'CV Markdown',
      description: [
        'Mon site perso en version simplifiée'
      ],
      video: '',
      technologies: ['Markdown'],
      image: 'assets/img/CVMarkdown.jpg',
      gitlab: {
        code: 'https://gitlab.com/mustapha_el_khalfaoui/ionicrecettes'
      },
      link: 'http://txti.es/mustapha-elkhalfaoui/images'
    }
  ];
  constructor() {}

  ngOnInit(): void {}
}
