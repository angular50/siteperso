import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ResumeService {
  public experience = [
    {
      company: {
        name: 'DOMOTI SAS',
        url: '',
        location: 'Marquette-Lez-Lille',
        description:
          'Société de Vente Par Correspondance, 6 enseignes 5 pays, Equipement de la maison'
      },
      position: 'Traffic Manager / Chargé WebMarketing / Web Analyste',
      date: '2009-2019',
      tasks: [
        'Gestion et suivi des comparateurs de Prix / MarketPlace / Ventes Privées / Adwords / SEO / Campagnes Affiliation',
        'Mise en place de l\'analytics avec plan de taggage',
        'Reporting de l\’activité, analyse des kpi\’s et WebAnalytics\r',
        'Refonte des différents sites et outils internes d\’administration',
        'Gestion des campagnes d\’e-mailing et animation des sites'
      ],
      sites: [
        {
          name: 'TempsL /',
          url: 'https://tempsl.fr/',
        },
        {
          name: 'Confort&Vie /',
          url: 'https://www.confortetvie.fr/fr/index.aspx',
        },
        {
          name: 'MarianneMélodie /',
          url: 'https://www.mariannemelodie.fr/fr/index.aspx',
        },
        {
          name: 'IdéesHomme',
          url: 'https://www.ideeshomme.fr/fr/index.aspx',
        },
      ],
    },
    {
      company: {
        name: 'AERONET',
        url: '',
        location: 'Marcq-en-Baroeul',
        description:
          'Agence WEB spécialisée en référencement naturel et netlinking'
      },
      position: 'Consultant référenceur',
      date: '2009',
      tasks: [
        'Préconisation au référencement',
        'Analyse et suivi des positions d’expressions',
        'Netlinking et rédaction de contenu'
      ],
      sites: [
        {
          name: 'Divao /',
          url: 'http://www.divao.com/tee-shirt/',
        },
        {
          name: 'Espagnauto /',
          url: 'https://www.espagnauto.com/',
        },
        {
          name: 'Ducatillon',
          url: 'https://www.ducatillon.com/',
        },
      ],
    },
    {
      company: {
        name: 'ETO DIGITAL',
        url: '',
        location: 'Roubaix',
        description:
          'Agence Web spécialisée en déploiement de projets e-commerce'
      },
      position: 'Chef de Projet Marketing WEB',
      date: '2007-2008',
      tasks: [
        'Rédaction de cahier des charges fonctionnel',
        'Animation des équipes de production',
        'Animation commerciale online',
        'Suivi des équipes de développement et des prestataires externes'
      ],
      sites: [
        {
          name: 'ElectroDepot / ',
          url: 'https://www.electrodepot.fr/',
        },
        {
          name: 'Gigastore / ',
          url: '',
        },
        {
          name: 'Foir’Fouille / ',
          url: 'https://www.lafoirfouille.fr/',
        },
        {
          name: 'Listesetplaisirs / ',
          url: 'http://www.listesetplaisirs.fr/',
        },
        {
          name: 'Isostar',
          url: 'https://www.isostar.fr/',
        },
      ],
    },
  ];

  public education = [
    {
      degree: 'Master 2 - Management de Projet E-Business',
      school: { name: 'Université Lille 2 IMMD | 2009' }
    },
    {
      degree: 'Licence 3 - Chef de Projet Informatique',
      school: { name: 'Efficom Lille | 2007' }
    },
    {
      degree: 'Licence & Master 1 - Management de la Distribution',
      school: { name: 'Université Lille 2 IMMD | 2006' }
    },
    {
      degree: 'BTS - Commerce International',
      school: { name: 'Lycée Edouard Herriot | 2003' }
    },
  ];

  public formation = [
    {
      titre: 'Développeur FullStack',
      organisme: { name: 'Global Knowledge | 04/2020 - 06/2020 online' },
      projets: [
        {
          name: 'Gestibank',
          url: '',
        },
      ],
    },
    {
      titre: 'Développeur Java JEE',
      organisme: { name: 'AFPA & ASTON Euratechnologies | 10/2019 - 03/2020' },
      projets: [
        {
          name: 'JavAlumni',
          url: '',
        },
      ],
    }
  ];

  constructor() {}
}
