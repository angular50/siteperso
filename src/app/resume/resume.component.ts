import { Component, OnInit } from '@angular/core';
import { ResumeService } from './resume.service';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent implements OnInit {
  public experience = [];
  public education = [];
  public formation = [];

  constructor(private resumeService: ResumeService) {
    this.experience = this.resumeService.experience;
    this.formation = this.resumeService.formation;
    this.education = this.resumeService.education;
  }

  ngOnInit(): void {
    console.log(this.experience);
  }
}
