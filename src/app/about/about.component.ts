import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  skills = [
    { name: 'HTML', level: '95' },
    { name: 'CSS', level: '85' },
    { name: 'Javascript', level: '80' },
    { name: 'JAVA', level: '60' },
    { name: 'Typescript', level: '80' },
    { name: 'Angular', level: '80' },
    { name: 'Node', level: '70' },
    { name: 'Git', level: '85' },
    { name: 'SQL', level: '70' }
    ];
  coursesSololearn = [
    {
      title: 'HTML Fundamentals',
      certificate: 'https://www.sololearn.com/Certificate/1014-17287331/pdf/',
      url: 'https://www.sololearn.com/Play/HTML',
      gitlab: '',
      imgUrl: 'assets/img/html.png'
    },
    {
      title: 'CSS Fundamentals',
      certificate: 'https://www.sololearn.com/Certificate/1023-17287331/pdf/',
      url: 'https://www.sololearn.com/Play/CSS',
      gitlab: '',
      imgUrl: 'assets/img/css.png'
    },
    {
      title: 'JavaScript Tutorial',
      certificate: 'https://www.sololearn.com/Certificate/1024-17287331/pdf/',
      url: 'https://www.sololearn.com/Play/JavaScript',
      gitlab: '',
      imgUrl: 'assets/img/js.png'
    },
    {
      title: 'jQuery Tutorial',
      certificate: 'https://www.sololearn.com/Certificate/1082-17287331/pdf/',
      url: 'https://www.sololearn.com/Play/jQuery',
      gitlab: '',
      imgUrl: 'assets/img/jquery.png'
    },
    {
      title: 'Java Tutorial',
      certificate: 'https://www.sololearn.com/Certificate/1068-17287331/pdf/',
      url: 'https://www.sololearn.com/Play/Java',
      gitlab: '',
      imgUrl: 'assets/img/java.png'
    },
    {
      title: 'SQL Fundamentals',
      certificate: 'https://www.sololearn.com/Certificate/1060-17287331/pdf/',
      url: 'https://www.sololearn.com/Play/Sql',
      gitlab: '',
      imgUrl: 'assets/img/sql.png'
    },

  ];

  coursesGoogle = [
    {
      title: 'Certification Google Analytics',
      certificate: 'https://skillshop.exceedlms.com/student/award/30950524',
      url: 'https://skillshop.exceedlms.com/student/path/2947-certification-google-analytics-individual-qualification',
      gitlab: '',
      imgUrl: 'assets/img/ga.png'
    },
    {
      title: 'Google Analytics for Beginners',
      certificate: 'https://analytics.google.com/analytics/academy/certificate/Y64mh4fpScKI3X6Y8OhyZA',
      url: 'https://analytics.google.com/analytics/academy/',
      gitlab: '',
      imgUrl: 'assets/img/ga.png'
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
